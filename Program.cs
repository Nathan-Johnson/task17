﻿using System;

namespace task17
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Enter a character");
            char c = (char)Console.Read();
            if (Char.IsLetter(c))
            {
                if (Char.IsLower(c))
                {
                    Console.WriteLine("The Character is lowercase");
                }
                else
                {
                    Console.WriteLine("The Character is uppercase");
                }
            }
            else
            {
                Console.WriteLine("The character isn't an alphabetic character");
            }

        }
    }
}
